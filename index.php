<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animals("shaun");

echo "Name : " . $sheep->nama; // "shaun"
echo "<br>";
echo "Legs: " . $sheep->legs; // 4
echo "<br>";
echo "Cool Blooded : " . $sheep->cold_blooded; // "no"
echo "<hr>";

// index.php
$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->nama; // "shaun"
echo "<br>";
echo "Legs: " . $sungokong->legs; // 4
echo "<br>";
echo "Cool Blooded : " . $sungokong->cold_blooded; // "no"
echo "<br>";
$sungokong->yell(); // "Auooo"
echo "<hr>";

$kodok = new Frog("buduk");
echo "Name :" . $kodok->nama; // "shaun"
echo "<br>";
echo "Legs: " . $kodok->legs; // 4
echo "<br>";
echo "Cool Blooded : " . $kodok->cold_blooded; // "no"
echo "<br>";
$kodok->jump(); // "hop hop"
